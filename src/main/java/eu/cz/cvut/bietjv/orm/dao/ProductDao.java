package eu.cz.cvut.bietjv.orm.dao;

import eu.cz.cvut.bietjv.orm.model.Product;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class ProductDao {

    @PersistenceContext
    private EntityManager em;

    public void persist(Product product) {
        em.persist(product);
    }

    public List findAll() {

        return em.createQuery("select P FROM Product P").getResultList();
    }

}
